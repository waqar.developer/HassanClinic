
NProgress.start();

// Increase randomly
var interval = setInterval(function() { NProgress.inc(); }, 8000);

// Trigger finish when page fully loaded
jQuery(window).load(function () {
    clearInterval(interval);
    NProgress.done();
});

// Trigger bar when exiting the page
jQuery(window).unload(function () {
    NProgress.start();
})




var getUrl = window.location;
var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+"/web/";





function addnew(event,obj) {

    event.preventDefault();
    var url = obj.getAttribute("href");
    console.log(obj.text);
    var dialog = bootbox.dialog({
        title: obj.text,
        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',

    });

    dialog.init(function(){
        var request = $.ajax({
            url: url,
            method: "GET",
        });

        request.done(function( msg ) {
            dialog.find('.bootbox-body').html(msg);
        });

        $(document).on("submit", "#form", function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();


            $form = $(this); //wrap this in jQuery

            var url = $form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: $("#form").serialize(),
                // serializes the form's elements.
                success: function(data)
                {
                    if(data==true)
                    {
                        $.pjax.reload({container:'#p0'});
                        bootbox.hideAll();
                    }


                }
            });
            // avoid to execute the actual submit of the form.

        });

    });

};



function updateRecord(id,controller,title,event) {

    event.preventDefault();
    var dialog = bootbox.dialog({
        title: title,
        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',


    });


    dialog.init(function(){
        var request = $.ajax({
            url: baseUrl+controller+"/update?id="+id,
            method: "GET",
        });

        request.done(function( msg ) {
            dialog.find('.bootbox-body').html(msg);


        });

        $(document).on("submit", "#form", function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();

            $form = $(this); //wrap this in jQuery

            var url = $form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: $("#form").serialize(),
                // serializes the form's elements.
                success: function(data)
                {
                    if(data==true)
                    {
                        //toastr.success('', 'Update Successfully', {timeOut: 2000});
                        $.pjax.reload({container:'#p0'});
                        bootbox.hideAll();
                    }
                    else
                    {
                        //toastr.success('', 'Some Error Occur', {timeOut: 2000});
                    }


                }
            });
            // avoid to execute the actual submit of the form.
        });

    });

}

$('.popup-gallery').magnificPopup({
    delegate: 'a',
    type: 'image',
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
    },
    image: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
    }
});

