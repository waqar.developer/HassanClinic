<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "files".
 *
 * @property int $id
 * @property string $file_name
 * @property int $checkin_id
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 * @property string $custom_name
 * @property string $type
 * @property string $size
 */
class Files extends \yii\db\ActiveRecord
{

    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file_name', 'created_on', 'created_by'], 'required'],
            [['checkin_id', 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['file_name'], 'string', 'max' => 100],
            [['custom_name'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 50],
            [['size'], 'string', 'max' => 20],
            [['file'], 'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_name' => 'File Name',
            'checkin_id' => 'Check IN',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            'custom_name' => 'Custom Name',
            'type' => 'Type',
            'size' => 'Size',
            'file'=>'File',
        ];
    }
}
