<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $config_item_name
 * @property string $config_item_value
 * @property string $config_item_description
 * @property int $active
 * @property string $created_on
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['config_item_name', 'config_item_value', 'config_item_description', 'active', 'created_on'], 'required'],
            [['config_item_value'], 'string'],
            [['active'], 'integer'],
            [['created_on'], 'safe'],
            [['config_item_name', 'config_item_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'config_item_name' => 'Config Item Name',
            'config_item_value' => 'Config Item Value',
            'config_item_description' => 'Config Item Description',
            'active' => 'Active',
            'created_on' => 'Created On',
        ];
    }

    public static function findByName($config_item_name)

    {

        return static::findOne ( [

            'config_item_name' => $config_item_name

        ] );

    }
}
