<?php

use kartik\password\PasswordInput;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<style>

    .control-label{
        font-size: 0.9rem !important;
        color: black;
    }
</style>


<div class="user-form">

    <?php

    $form = ActiveForm::begin([
        'id' => 'form',
        'enableAjaxValidation' => true,
        'validationUrl' => ($model->isNewRecord) ?Yii::$app->homeUrl.'user/validate':Yii::$app->homeUrl.'user/validate?id='.$model->id.'&change-password=true',
        'errorCssClass' => 'has-danger',
    ]);
    ?>


    <div class="row">
        <div class="col-md-12">
            <?php
            echo $form->field($model, 'password')->widget(
                PasswordInput::classname()
            );

            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
            echo $form->field($model, 'repeat_password')->widget(
                PasswordInput::classname()
            );

            ?>
        </div>
    </div>



    <div class="form-group">
        <?= Html::submitButton('Change Password', ['class' => 'btn btn-info']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
