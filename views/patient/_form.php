<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Patient */
/* @var $form yii\widgets\ActiveForm */
$type  = ['Wife'=>'Wife','Son'=>'Son','Daughter'=>'Daughter','Other'=>'Other'];

$gender  = ['Male'=>'Male','Female'=>'Female','Other'=>'Other'];

?>

<div class="patient-form">

    <?php
    $form = ActiveForm::begin([
        'id' => 'form',
        'enableAjaxValidation' => true,
        'validationUrl' => ($model->isNewRecord) ?Yii::$app->homeUrl.'patient/validate':Yii::$app->homeUrl.'patient/validate?id='.$model->id.'',
        'errorCssClass' => 'has-danger',
    ]);
    ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'reg_no')->textInput(['maxlength' => true,'readonly'=> true]) ?>

            <?= $form->field($model, 'age')->textInput() ?>

            <?= $form->field($model, 'gender')->dropDownList($gender, ['prompt' => '']) ?>

            <?php // $form->field($model, 'email')->textInput(['maxlength' => true]) ?>


        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'phone_no')->textInput(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'relationship')->dropDownList($type, ['prompt' => '']) ?>

                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'relationship_of')->textInput(['maxlength' => true]) ?>

                </div>
            </div>



        <?php // $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

        <?php // $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

        <?= $form->field($model, 'address')->textarea(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
