<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LeadStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */




$this->title = 'Country';
$this->params['breadcrumbs'][] = $this->title;




function statusLabel($status)
{
    if ($status !='1')
    {
        $label = "<span class=\"highlight-danger\">".Yii::t('app', 'Inactive')."</span>";
    }
    else
    {
        $label = "<span class=\"highlight\">".Yii::t('app', 'Active')."</span>";
    }
    return $label;
}
$status = array('0'=>Yii::t('app', 'Inactive'),'1'=>Yii::t('app', 'Active'));




?>
<div class="country-index">
    <?php Pjax::begin(); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'toolbar' =>  [
            //'{export}',
            '{toggleData}',
        ],

        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
            'before' => Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New Country'), [
                'create'
            ], ['data-pjax' => 0,'onclick'=>'addnew(event,this)',
                'class' => 'btn btn-default btn-sm add-new'
            ]),
            'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                    'index'
                ], [
                    'class' => 'btn btn-primary btn-sm'
                ]),


            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'country',
            'country_code',
            //'active',
            [
                'attribute'=>'active',
                'value'=>function($model)
                {
                    return statusLabel($model->active);
                },
                'format'=>'raw',
            ],
            //'sort_order',
            'created_on',
            'created_by',
            //'updated_on',
            //'updated_by',

            [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{update} {view}  {defaultValue}',
                'buttons' => [
                    'update' => function ($url, $model)

                    {
                        return '<a href="#"><span class="fa fa-pencil-alt"  onclick="updateRecord('.$model->id.',\'country\',\'Update Country\',event)"></span></a>';
                    } ,
                    'defaultValue' => function ($url, $model) {
                        if(\app\models\DefaultValueModule::checkDefaultValue('country',$model->id)){
                            return Html::a('<span class="fa fa-eraser"></span>', Yii::$app->urlManager->createUrl(['country/index','del_id' => $model->id]), [
                                'title' => Yii::t('app', 'Delete Default'),
                            ]);
                        }else{
                            return Html::a('<span class="fa fa-tag"></span>', Yii::$app->urlManager->createUrl(['country/index','id' => $model->id]), [
                                'title' => Yii::t('app', 'Make Default'),
                            ]);
                        }
                    }

                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>


</div>
