<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PatientSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="patient-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'reg_no')->textInput(['maxlength' => 255, 'class' => 'form-control form-control-sm mb-3']); ?>

        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => 255, 'class' => 'form-control form-control-sm mb-3']); ?>

        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'phone_no')->textInput(['maxlength' => 255, 'class' => 'form-control form-control-sm mb-3']); ?>
        </div>
    </div>



    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'gender') ?>

    <?php // echo $form->field($model, 'age') ?>

    <?php // echo $form->field($model, 'relationship') ?>

    <?php // echo $form->field($model, 'whatsapp_no') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'referred_by_id') ?>

    <?php // echo $form->field($model, 'panel_id') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_on') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_on') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-sm']) ?>
        <a href="<?= Yii::$app->homeUrl?>site/index"  class="btn btn-default btn-sm">Reset</a>


        <button onclick="add_patient(event,this)" class="btn btn-success btn-sm">Add New Patient</button>

    </div>

    <?php ActiveForm::end(); ?>

</div>
