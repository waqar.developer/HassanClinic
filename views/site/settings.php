<?php

$this->title = 'Settings';

?>

<div class="row">
    <div class="col-lg-12">
        <section class="card">
            <form action="<?= Yii::$app->homeUrl?>site/settings-update" method="post">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />


                <header class="card-header">
                <div class="card-actions" style="top:6px;">
                    <button type="submit" class="mb-1 mt-1 mr-1 btn btn-primary">Save</button>

                </div>
                <h2 class="card-title">Project Category List</h2>
            </header>
            <div class="card-body">


                <?php

                $project_category = explode(',', Yii::$app->params['PROJECT_CATEGORY'] );
                $i = 0;
                ?>

                <div class="dynamic_project_category">

                    <?php foreach ($project_category as $category)
                    {?>

                        <div class="mb-3 form-inline">
                            <input type="text" name="project_category[]" class="form-control mb-2 mr-sm-2 mb-sm-0" value="<?=$category?>" required>
                            <?php echo $i>0?'<button class="btn btn-danger btn-sm remove_project_category">Remove</button>':'<button  class="btn btn-primary btn-sm add_field_project_category">Add New</button>'; ?>


                        </div>

                   <?php $i++; } ?>



                </div>

            </div>
            </form>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="card">
            <form action="<?= Yii::$app->homeUrl?>site/settings-update" method="post">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />


                <header class="card-header">
                    <div class="card-actions" style="top:6px;">
                        <button type="submit" class="mb-1 mt-1 mr-1 btn btn-primary">Save</button>

                    </div>
                    <h2 class="card-title">Inventory Purpose</h2>
                </header>
                <div class="card-body">

                    <?php

                    $inventory_purpose = explode(',', Yii::$app->params['PURPOSE'] );
                    $i = 0;
                    ?>

                    <div class="dynamic_inventory_purpose">

                        <?php foreach ($inventory_purpose as $purpose)
                        {?>

                            <div class="mb-3 form-inline">
                                <input type="text" name="inventory_purpose[]" class="form-control mb-2 mr-sm-2 mb-sm-0" value="<?=$purpose?>" required>
                                <?php echo $i>0?'<button class="btn btn-danger btn-sm remove_inventory_purpose">Remove</button>':'<button  class="btn btn-primary btn-sm add_field_inventory_purpose">Add New</button>'; ?>


                            </div>

                            <?php $i++; } ?>



                    </div>

                </div>
            </form>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="card">
            <form action="<?= Yii::$app->homeUrl?>site/settings-update" method="post">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />


                <header class="card-header">
                    <div class="card-actions" style="top:6px;">
                        <button type="submit" class="mb-1 mt-1 mr-1 btn btn-primary">Save</button>

                    </div>
                    <h2 class="card-title">Property Heading Category</h2>
                </header>
                <div class="card-body">

                    <?php

                    $property_heading_category = explode(',', Yii::$app->params['PROPERTY_HEADING_CATEGORY'] );
                    $i = 0;
                    ?>

                    <div class="dynamic_property_heading_category">

                        <?php foreach ($property_heading_category as $category)
                        {?>

                            <div class="mb-3 form-inline">
                                <input type="text" name="property_heading_category[]" class="form-control mb-2 mr-sm-2 mb-sm-0" value="<?=$category?>" required>
                                <?php echo $i>0?'<button class="btn btn-danger btn-sm remove_property_heading_category">Remove</button>':'<button  class="btn btn-primary btn-sm add_field_property_heading_category">Add New</button>'; ?>


                            </div>

                            <?php $i++; } ?>



                    </div>

                </div>
            </form>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="card">
            <form action="<?= Yii::$app->homeUrl?>site/settings-update" method="post">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />


                <header class="card-header">
                    <div class="card-actions" style="top:6px;">
                        <button type="submit" class="mb-1 mt-1 mr-1 btn btn-primary">Save</button>

                    </div>
                    <h2 class="card-title">Property Category</h2>
                </header>
                <div class="card-body">

                    <?php

                    $property_category = Yii::$app->params['PROPERTY_CATEGORY'];

                    $data = json_decode($property_category, true);
/*
                   echo "<pre>";
                    print_r($data);
                    echo "</pre>";*/

                    $i = 0;
                    ?>

                    <div class="dynamic_property_category">

                        <?php foreach ($data['property_category'] as $key=>$category)



                        {?>

                            <div class="mb-3 form-inline">


                                <select class="form-control mr-sm-2 mb-sm-0" name="property_type[]" required>
                                    <option value="">Select Category</option>
                                    <?php foreach ($property_heading_category as $category2){
                                        if($category['code']==$category2){?>
                                    <option value="<?= $category2?>" selected><?= $category2?></option>

                                  <?php  }else { ?>

                                    <option value="<?= $category2?>"><?= $category2?></option>
                                            <?php } ?>
                                    <?php } ?>
                                </select>



                                <input type="text" name="property_category[]" class="form-control mb-2 mr-sm-2 mb-sm-0" value="<?=$category['name'];?>" required>

                                <?php echo $i>0?'<button class="btn btn-danger btn-sm remove_property_category">Remove</button>':'<button  class="btn btn-primary btn-sm add_field_property_category">Add New</button>'; ?>


                            </div>

                            <?php $i++; } ?>



                    </div>

                </div>
            </form>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="card">
            <form action="<?= Yii::$app->homeUrl?>site/settings-update" method="post">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />


                <header class="card-header">
                    <div class="card-actions" style="top:6px;">
                        <button type="submit" class="mb-1 mt-1 mr-1 btn btn-primary">Save</button>

                    </div>
                    <h2 class="card-title">Property Land Area Unit</h2>
                </header>
                <div class="card-body">

                    <?php

                    $property_unit = Yii::$app->params['PROPERTY_LAND_AREA_UNIT'];

                    $data = json_decode($property_unit, true);
                    /*
                                       echo "<pre>";
                                        print_r($data);
                                        echo "</pre>";*/

                    $i = 0;
                    ?>

                    <div class="dynamic_property_unit">

                        <?php foreach ($data['property_land_area_unit'] as $key=>$category)



                        {?>

                            <div class="mb-3 form-inline">


                                <input type="text" name="property_unit[]" class="form-control mb-2 mr-sm-2 mb-sm-0" value="<?=$category['unit'];?>" required>

                                <input type="text" name="property_unit_name[]" class="form-control mb-2 mr-sm-2 mb-sm-0" value="<?=$category['name'];?>" required>

                                <?php echo $i>0?'<button class="btn btn-danger btn-sm remove_property_unit">Remove</button>':'<button  class="btn btn-primary btn-sm add_field_property_unit">Add New</button>'; ?>


                            </div>

                            <?php $i++; } ?>



                    </div>

                </div>
            </form>
        </section>
    </div>
</div>


<script>

    $(document).ready(function() {
        var max_fields      = 5; //maximum input boxes allowed
        var wrapper         = $(".dynamic_project_category"); //Fields wrapper
        var add_button      = $(".add_field_project_category"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class=" mb-3 form-inline">'+
                    '<label class="sr-only" for="inlineFormInputName2">Name</label>'+
                    '<input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" name="project_category[]" required>'+

                    '<button class="btn btn-danger btn-sm remove_project_category">Remove</button>'+
                    '</div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_project_category", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })

    });


    $(document).ready(function() {
        var max_fields      = 5; //maximum input boxes allowed
        var wrapper         = $(".dynamic_inventory_purpose"); //Fields wrapper
        var add_button      = $(".add_field_inventory_purpose"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class=" mb-3 form-inline">'+
                    '<input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" name="inventory_purpose[]" required>'+

                    '<button class="btn btn-danger btn-sm remove_inventory_purpose">Remove</button>'+
                    '</div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_inventory_purpose", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })

    });

    $(document).ready(function() {


        var wrapper         = $(".dynamic_property_heading_category"); //Fields wrapper
        var add_button      = $(".add_field_property_heading_category"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();

            x++; //text box increment
            $(wrapper).append('<div class=" mb-3 form-inline">'+
                '<input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" name="property_heading_category[]" required>'+

                '<button class="btn btn-danger btn-sm remove_property_heading_category">Remove</button>'+
                '</div>'); //add input box

        });

        $(wrapper).on("click",".remove_property_heading_category", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })

    });

    $(document).ready(function() {

        var wrapper         = $(".dynamic_property_category"); //Fields wrapper
        var add_button      = $(".add_field_property_category"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();

                x++; //text box increment
                $(wrapper).append('<div class=" mb-3 form-inline">'+
                        '<select class="form-control mr-sm-2 mb-sm-0" name="property_type[]" required>'+
                    '<option value="">Select Category</option>'+
                    <?php foreach ($property_heading_category as $category)
                    {?>

            '<option value="<?=$category?>"><?=$category?></option>'+
                    <?php } ?>
            '</select>'+
                    '<input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" name="property_category[]" required>'+

                    '<button class="btn btn-danger btn-sm remove_property_category">Remove</button>'+
                    '</div>'); //add input box

        });

        $(wrapper).on("click",".remove_property_category", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })

    });

    $(document).ready(function() {

        var wrapper         = $(".dynamic_property_unit"); //Fields wrapper
        var add_button      = $(".add_field_property_unit"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();

            x++; //text box increment
            $(wrapper).append('<div class=" mb-3 form-inline">'+
                '<input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" name="property_unit[]" required>'+
                '<input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" name="property_unit_name[]" required>'+

                '<button class="btn btn-danger btn-sm remove_property_unit">Remove</button>'+
                '</div>'); //add input box

        });

        $(wrapper).on("click",".remove_property_unit", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })

    });


</script>