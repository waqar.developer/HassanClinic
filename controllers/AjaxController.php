<?php

namespace app\controllers;

use app\models\Patient;
use Yii;
use app\models\Country;
use app\models\CountrySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class AjaxController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionGetPatient()
    {
        $query = $_GET['id'];
        $value = Patient::findOne($query);

        $patient_array = array();

            $patient['id'] = $value->id;
            $patient['name'] = $value->name;
            $patient['reg_no'] = $value->reg_no;
            $patient['phone_no'] = $value->phone_no;
            $patient['age'] = $value->age.' Year';
        if($value->relationship)
        {
            $patient['relation'] = $value->relationship.' ('.$value->relationship_of.')';
        }else
        {
            $patient['relation'] = '';
        }

            $patient['gender'] = $value->gender;
            

        echo json_encode($patient);
    }

    public function actionSearchPatient()
{
    $query = $_GET['q'];
    $model = Patient::find()->orWhere(['like','name',$query])->orWhere(['like','reg_no',$query])->orWhere(['like','phone_no',$query])->all();

    $patient_array = array();
    foreach ($model as $key=>$value)
    {
        $patient['id'] = $value['id'];
        $patient['name'] = $value['name'];
        $patient['reg_no'] = $value['reg_no'];
        $patient['phone_no'] = $value['phone_no'];
        $patient['age'] = $value['age'];
        $patient['relation'] = $value['relationship'];
        $patient['gender'] = $value['gender'];
        $patient_array[] = $patient;
    }


    $response['patient'] = $patient_array;

    echo json_encode($patient_array);
}
}
