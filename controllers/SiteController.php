<?php

namespace app\controllers;

use app\models\CheckIn;
use app\models\Files;
use app\models\PatientSearch;
use app\models\Settings;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
   
     public function behaviors()
    {

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['login'],
                    'allow' => true,
                ],
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='site/logout' || $route=='site/validate-complain')
                        {
                            return true;
                        }
                       else if (\Yii::$app->user->can($route)) {
                            return true;
                        }else{
                           return true;
                       }

                    }
                ],
            ],
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'logout' => ['GET'],
            ]
        ];


        return $behaviors;
    }


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new CheckIn();

        $model->created_by = Yii::$app->user->id;
        $model->created_on = date("Y-m-d H:i:s");
        $model->status  = "1";

        $regNo = CheckIn::find()->select('invoice_no')->orderBy(['id'=> SORT_DESC])->one();
        $num = $regNo['invoice_no'];
        $num++;
        $model->invoice_no = $num;
        
        

        $searchModel = new PatientSearch();
        $dataProvider = $searchModel->searchCustom(Yii::$app->request->queryParams);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            echo $model->id;
            exit;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


    // action examples

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    public function actionFileUpload()
    {
        $model = new Files();

        $model->file = $_FILES['file'];

        $model->custom_name = $_POST['custom_name'][0];

        if (isset($_GET['check-in']))
        {
            $checkin = CheckIn::findOne($_GET['check-in']);
            $model->checkin_id = $checkin->id;

        }

        $directory = Yii::getAlias('@app/files') . DIRECTORY_SEPARATOR . $checkin->patient->reg_no . DIRECTORY_SEPARATOR;
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }



        if ($files = UploadedFile::getInstance($model,'file')) {
            $model->file_name = $files->baseName.'.'.$files->extension;
            $model->type = $files->type;



            $model->size = "".$files->size;
            //$fileName = $model->file->type;
            $filePath = $directory . $model->file_name ;

            //echo $model->file['type'];

            if ($files->saveAs($filePath)) {
                $model->created_by = Yii::$app->user->id;
                $model->created_on = date("Y-m-d H:i:s");
                $model->save();
                $path = '../../files/'. $checkin->patient->reg_no.'/'.$model->file_name ;
                $a = $model->type;

                if (strpos($a, 'images') !== false) {
                   $thumb = '../../files/'. $checkin->patient->reg_no.'/'.$model->file_name ;
                }
                else
                {
                    $thumb = Yii::$app->homeUrl.'images/file.png';
                }

                return Json::encode([


                    'files' => [
                        [
                            'id'=>$model->id,
                            'name' => $model->file_name,
                            'size' => (float)$model->size,
                            'custom_name'=>$model->custom_name,
                            'url' => $path,
                            'thumbnailUrl' => $thumb,
                            'deleteUrl' => Yii::$app->homeUrl.'site/file-remove?name=' . $model->id,
                            'deleteType' => 'GET',
                        ],
                    ],
                ]);
            }
        }
        else
        {
            echo "Failed";
        }

        return '';
    }

    public function actionFileRemove()
    {
        $id = $_GET['name'];
        $model = Files::findOne($id);
        $name= $model->file_name;
        $model->delete();
        $directory = Yii::getAlias('@app/files') . DIRECTORY_SEPARATOR . Yii::$app->user->id . DIRECTORY_SEPARATOR;

        if (is_file($directory . DIRECTORY_SEPARATOR . $name)) {
            unlink($directory . DIRECTORY_SEPARATOR . $name);
        }

        $files = FileHelper::findFiles($directory);
        $output = [];
        foreach ($files as $file) {
            $fileName = basename($file);
            $path = '../../files/'. Yii::$app->user->id.'/'.$fileName ;
            $output['files'][] = [
                'name' => $fileName,
                'size' => filesize($file),
                'url' => $path,
                'thumbnailUrl' => $path,
                'deleteUrl' => 'site/file-remove?name=' . $fileName,
                'deleteType' => 'POST',
            ];
        }
        return Json::encode($output);

    }
    
    
    
    public function actionSettings()
    {

        $items = Settings::find()->asArray()->all();
        foreach ($items as $item)
        {
            if ($item['config_item_name'])
            {
                Yii::$app->params[$item['config_item_name']] = $item['config_item_value'];
                Yii::$app->params[$item['config_item_name']."_description"] = $item['config_item_description'];
            }
        }
        
        return $this->render('settings');
    }


    public function actionSettingsUpdate()
    {
        if(isset($_POST['project_category']))
        {
            $project_category = implode (",", $_POST['project_category']);
            $model = Settings::findByName('PROJECT_CATEGORY');
            $model->config_item_value = $project_category;
            $model->save();

        }
        if(isset($_POST['inventory_purpose']))
        {
            $inventory_purpose = implode (",", $_POST['inventory_purpose']);
            $model = Settings::findByName('PURPOSE');
            $model->config_item_value = $inventory_purpose;
            $model->save();

        }

        if(isset($_POST['property_heading_category']))
        {
            $property_heading_category = implode (",", $_POST['property_heading_category']);
            $model = Settings::findByName('PROPERTY_HEADING_CATEGORY');
            $model->config_item_value = $property_heading_category;
            $model->save();

        }


        if(isset($_POST['property_category']) && $_POST['property_type'])
        {

            $type = $_POST['property_type'];
            $name = $_POST['property_category'];
            $response = array();
            foreach ($type as $index => $code)
            {

                $cat['code'] = $code;
                $cat['name'] = $name[$index];

                $response['property_category'][] = $cat;
            }

            $property_data =  json_encode($response);
            $model = Settings::findByName('PROPERTY_CATEGORY');
            if($model != null)
            {
                $model->config_item_value = $property_data;
                $model->save();
            }
        }

        if(isset($_POST['property_unit']) && $_POST['property_unit_name']) {

            $type = $_POST['property_unit'];
            $name = $_POST['property_unit_name'];
            $response = array();
            foreach ($type as $index => $code) {

                $cat['unit'] = $code;
                $cat['name'] = $name[$index];

                $response['property_land_area_unit'][] = $cat;
            }

            $property_data = json_encode($response);
            $model = Settings::findByName('PROPERTY_LAND_AREA_UNIT');
            if ($model != null) {
                $model->config_item_value = $property_data;
                $model->save();
            }


        }


        return $this->redirect(['settings']);
    }
    
    
    public function actionPrintBill()
    {
        $id = $_GET['id'];
        $model = CheckIn::findOne($id);

        return $this->renderPartial('print-bill', [
            'model' => $model,
        ]);
        
    }



}
